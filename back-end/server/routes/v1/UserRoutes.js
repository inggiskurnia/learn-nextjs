const UserController = require('../../controllers/UserController');
const userRouter = require('express').Router();

/**
 * @Routes "/api/v1/users"
 */

userRouter.get('/users', UserController.getUsers);
userRouter.post('/register', UserController.register);
userRouter.get('/profile/:id', UserController.getUserById);
userRouter.put('/profile/:id', UserController.updateUser);
userRouter.delete('/users/:id', UserController.deleteUser);

module.exports = userRouter;
